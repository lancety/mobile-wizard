import {topic, subtopics, Topic} from "./data/sample";
import {combineReducers} from "Redux";

export const UPDATE_COMPLETED = "UPDATE_COMPLETED";

export const updateCompletedAction = (topic: Topic) => {
	return {
		type: UPDATE_COMPLETED,
		topic: topic
	}
};

// no need to filter these data, so simply default sample data
const reducer = {
	topic: (state: Topic = topic, action: any) => topic,
	subtopics: (state: Topic[] = subtopics, action: any) => {
		switch (action.type) {
			case UPDATE_COMPLETED:
				let result = state.map((topic: Topic) => {
					let tmpTopic = topic;

					if (tmpTopic === action["topic"]) {
						tmpTopic = (Object as any).assign({}, topic, {
							completed: !topic.completed
						});
					}

					return tmpTopic;
				});

				return result;
			default:
				return state;
		}
	}
};

export default combineReducers(reducer);