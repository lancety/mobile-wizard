import * as React from "react";
import {Topic} from "../data/sample";
import TopicPanel from "./panel";

interface TopicPanelContProps {
	topics: Topic[];
	selectedIndex: number;
	updateCompleted: (topic: Topic) => void;
}

class TopicPanelCont extends React.Component<TopicPanelContProps, any> {
	private root: HTMLElement;
	private cont: HTMLElement;

	constructor(props: TopicPanelContProps, context: any) {
		super(props, context);
	}

	private componentDidMount() {
		this.root = this.refs["root"] as HTMLElement;
		this.cont = this.refs["cont"] as HTMLElement;
		this.componentDidUpdate();
	}

	private componentDidUpdate() {
		let translateX = (this.root.clientWidth - 300) / 2 - this.props.selectedIndex * 300;
		this.cont.style.transform = `translateX(${translateX}px)`;
	}

	render() {
		return <div className="topic-panel-root bg-white" ref="root">
			<div className="topic-panel-cont text-center" ref="cont">
				{
					this.props.topics.map((topic: Topic, index: number) => {
						return <TopicPanel
							key={index}
							topic={topic}
							isSelected={index===this.props.selectedIndex}
							updateCompleted={this.props.updateCompleted}/>
					})
				}

			</div>
		</div>
	}
}

export default TopicPanelCont;