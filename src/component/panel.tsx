import * as React from "react";
import {Topic} from "../data/sample";

interface TopicPanelProps {
	topic: Topic;
	isSelected: boolean;
	updateCompleted: (topic: Topic) => void;
}

class TopicPanel extends React.Component<TopicPanelProps, any> {
	private panel: HTMLElement;

	constructor(props: TopicPanelProps, context: any) {
		super(props, context);
	}

	componentDidMount(){
		this.panel = this.refs["panel"] as HTMLElement;
		this.componentDidUpdate();
	}

	componentDidUpdate() {
		if(this.props.isSelected) {
			this.panel.style.transform = "scale(1.05)";
		} else {
			this.panel.style.transform = "scale(1)";
		}
	}

	render() {
		return <div className="topic-panel" ref="panel">
			<div className="topic-panel-bg"/>
			<div className="topic-panel-force">
				<div className="topic-panel-title">{this.props.topic.index}. {this.props.topic.title}</div>
				<div className={`topic-panel-completed ${this.props.topic.completed ? "" : "hide"}`}>&#10003;</div>

				<br/>
				<div className="button btn-complete-it"
					 onClick={()=>{this.props.updateCompleted(this.props.topic)}}>
					{`Let's go`}
				</div>
			</div>
			<div className="expander"/>
		</div>
	}
}

export default TopicPanel;