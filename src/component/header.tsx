import * as React from "react";
import {Topic} from "../data/sample";
import ReactInstance = React.ReactInstance;

interface TopicHeaderProps {
	topics: Topic[];
	selectedIndex: number;
	updateSelected: (topicIndex: number) => void;
}

class TopicHeader extends React.Component<TopicHeaderProps, any> {
	private root: HTMLElement;
	private cont: HTMLElement;

	constructor(props: TopicHeaderProps, context: any) {
		super(props, context);
	}

	private updateSelected = (direction: string) => {
		if (direction.toLowerCase() === "l") {
			let index = this.props.selectedIndex;
			if (index > 0) {
				this.props.updateSelected(index - 1);
			} else {
				console.warn("already selected first topic");
			}
		}

		if (direction.toLowerCase() === "r") {
			let index = this.props.selectedIndex;
			if (index < this.props.topics.length - 1) {
				this.props.updateSelected(index + 1);
			} else {
				console.warn("already selected last topic");
			}
		}
	};

	private componentDidMount() {
		this.root = this.refs["root"] as HTMLElement;
		this.cont = this.refs["cont"] as HTMLElement;
	}

	private componentDidUpdate(){
		let circleCanDisplay = Math.floor(this.root.offsetWidth / 40);
		let translateX = (this.props.selectedIndex + 2 - circleCanDisplay) * 40;
		this.cont.style.transform = `translateX(-${translateX}px)`;
	}

	render() {
		return <div className="header-cont bg-pink">
			<div className="col-xs-2 header-nav"
				 onClick={()=>{this.updateSelected("l")}}>{"<"}</div>
			<div className="col-xs-8 header-circle-root" ref="root">
				<div className="header-circle-cont" ref="cont">
					{
						this.props.topics.map((topic: Topic, ind: number) => {
							let className = "header-circle";
							if (topic.completed) {
								className += " completed"
							}
							if (ind === this.props.selectedIndex) {
								className += " selected"
							}

							return <div key={ind}
										className={className}
										onClick={()=> {this.props.updateSelected(ind)}}>
								{topic.index}
							</div>
						})
					}
				</div>
			</div>
			<div className="col-xs-2 header-nav"
				 onClick={()=>{this.updateSelected("r")}}>{">"}</div>
			<div className="expander"/>
		</div>
	}
}

export default TopicHeader;