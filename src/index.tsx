import * as React from "react";
import {createStore, Store} from "redux";
import {render} from "react-dom"
import {Provider, connect, MapStateToProps, MapDispatchToPropsFunction} from "react-redux";

import {Topic, topic, subtopics} from "./data/sample";
import reducer from "./reducer";
import TopicHeader from "./component/header";
import TopicPanelCont from "./component/panel-cont";
import {updateCompletedAction} from "./reducer";

// main app component

interface AppProps {
	topic: Topic;
	subtopics: Topic[];
	dispatch: any;
}

interface AppState {
	selectedIndex: number
}

class App extends React.Component<AppProps, any> {
	constructor(props: AppProps, context: any) {
		super(props, context);

		this.state = {
			selectedIndex: 0
		} as AppState;
	}

	private updateSelected = (topicIndex: number) => {
		this.setState({
			selectedIndex: topicIndex
		} as AppState)
	};

	private updateCompleted = (topic: Topic) => {
		let index = this.props.subtopics.indexOf(topic);
		if (index === -1) {
			return;
		}

		this.props.dispatch(updateCompletedAction(topic));
	};

	render() {
		return <div>
			<div id="top" className="bg-pink">
				<div className="col-xs-2"><span className="min-menu">=</span></div>
				<div className="col-xs-8">{this.props.topic.title}</div>
				<div className="col-xs-2"><div id="avatar" alt="avatar"></div></div>
				<div className="expander"/>
			</div>
			<TopicHeader
				topics={this.props.subtopics}
				selectedIndex={this.state.selectedIndex}
				updateSelected={this.updateSelected}/>
			<TopicPanelCont
				topics={this.props.subtopics}
				selectedIndex={this.state.selectedIndex}
				updateCompleted={this.updateCompleted}/>
			<div id="foot">
				<div className="foot-menu-item col-xs-4">A</div>
				<div className="foot-menu-item col-xs-4">B</div>
				<div className="foot-menu-item col-xs-4">C</div>
			</div>
		</div>
	}
}

const mapStateToProps: MapStateToProps<AppProps, any> = (state: AppProps) => state;
const mapDispatchToProps: MapDispatchToPropsFunction<any, any> = (dispatch, ownProps) => {
	return {
		dispatch: dispatch
	}
};
let AppComponent = connect(mapStateToProps, mapDispatchToProps)(App);

// default root
const store: Store<{}> = createStore(reducer, {topic, subtopics} as any);

render(
	<Provider store={store}>
		<AppComponent/>
	</Provider>,
	document.getElementById("root")
);

